object FormPrincipal: TFormPrincipal
  Left = 336
  Top = 105
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Consulta de CNPJ'
  ClientHeight = 676
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 217
    Width = 633
    Height = 459
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 11
      Width = 98
      Height = 16
      Caption = 'Tipo de Empresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 112
      Top = 11
      Width = 77
      Height = 16
      Caption = 'Raz'#227'o Social'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 536
      Top = 11
      Width = 49
      Height = 16
      Caption = 'Abertura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 192
      Top = 59
      Width = 59
      Height = 16
      Caption = 'Endere'#231'o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 486
      Top = 59
      Width = 45
      Height = 16
      Caption = 'N'#250'mero'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 105
      Width = 80
      Height = 16
      Caption = 'Complemento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 344
      Top = 105
      Width = 34
      Height = 16
      Caption = 'Bairro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 8
      Top = 153
      Width = 40
      Height = 16
      Caption = 'Cidade'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 305
      Top = 153
      Width = 17
      Height = 16
      Caption = 'UF'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 344
      Top = 153
      Width = 27
      Height = 16
      Caption = 'CEP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 464
      Top = 153
      Width = 51
      Height = 16
      Caption = 'Situa'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 8
      Top = 59
      Width = 50
      Height = 16
      Caption = 'Fantasia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 8
      Top = 249
      Width = 90
      Height = 16
      Caption = 'CNAE Principal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 8
      Top = 299
      Width = 120
      Height = 16
      Caption = 'CNAE Secund'#225'rio(s)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 8
      Top = 201
      Width = 117
      Height = 16
      Caption = 'Endere'#231'o Eletr'#244'nico'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label18: TLabel
      Left = 344
      Top = 201
      Width = 47
      Height = 16
      Caption = 'Telefone'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel
      Left = 473
      Top = 11
      Width = 31
      Height = 16
      Caption = 'Porte'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object EditTipo: TEdit
      Left = 8
      Top = 29
      Width = 98
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object EditRazaoSocial: TEdit
      Left = 112
      Top = 29
      Width = 355
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object EditAbertura: TEdit
      Left = 536
      Top = 29
      Width = 88
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object EditEndereco: TEdit
      Left = 192
      Top = 77
      Width = 288
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
    object EditNumero: TEdit
      Left = 486
      Top = 77
      Width = 138
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object EditComplemento: TEdit
      Left = 8
      Top = 123
      Width = 330
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
    end
    object EditBairro: TEdit
      Left = 344
      Top = 123
      Width = 280
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
    end
    object EditCidade: TEdit
      Left = 8
      Top = 171
      Width = 289
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
    end
    object EditUF: TEdit
      Left = 305
      Top = 171
      Width = 33
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
    end
    object EditCEP: TEdit
      Left = 344
      Top = 171
      Width = 114
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
    end
    object EditSituacao: TEdit
      Left = 464
      Top = 171
      Width = 160
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
    end
    object EditFantasia: TEdit
      Left = 8
      Top = 77
      Width = 178
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object ListCNAE2: TListBox
      Left = 8
      Top = 318
      Width = 616
      Height = 125
      ItemHeight = 13
      TabOrder = 16
    end
    object EditCNAE1: TEdit
      Left = 8
      Top = 269
      Width = 616
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
    end
    object EditEmail: TEdit
      Left = 8
      Top = 219
      Width = 330
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
    end
    object EditTelefone: TEdit
      Left = 344
      Top = 219
      Width = 280
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      TabOrder = 14
    end
    object EditPorte: TEdit
      Left = 473
      Top = 29
      Width = 57
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 30
    Width = 633
    Height = 187
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 361
      Top = 12
      Width = 85
      Height = 16
      Caption = 'Digite o CNPJ:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 361
      Top = 78
      Width = 96
      Height = 16
      Caption = 'Digite o Captcha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object bbConsultar: TBitBtn
      Left = 504
      Top = 85
      Width = 98
      Height = 30
      Caption = 'Consultar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        E6040000424DE604000000000000360000002800000014000000140000000100
        180000000000B0040000120B0000120B00000000000000000000FFFFFFFDFDFD
        C1C1C1A7A7A7F9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D6C8C6C89F
        9F9FAEAEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEBECD4CBCEC9C7C89595
        95CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAE7E8D4CCCFBAB9B98D8D8D
        EDEDEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDCDDD3CBCEAEAEAE949494FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D0D2CDC7C99A9A9AB4B4B4FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2CCCEC6C0C2868686E3E3E3FFFFFF
        FBFBFBEDEDEDECECECF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFED7CFD1B6B3B3808081A2A2A2949496A3
        A3A49E9EA089898AABABABF3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B8B9B7B7B9E4E3E4FCFCFDFFFFFFFFFF
        FFFAFAFAD2D2D2838384E4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF1F1F1ADADAFF3F3F3F9F9F9F8F8F8FAFAFAFAFAFAFAFAFA
        FBFBFBECECEC878788F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFAFAFB1ECEBEBF3F3F3F4F4F4F6F6F6F6F6F6F7F7F7F6F6F6F6F6F6F8
        F8F8D8D8D9AAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8
        BAB9BBF0F0F0EFEFEFF4F4F4F4F4F4F4F4F4F5F5F5F4F4F4F5F5F5F4F4F4F6F6
        F6929293DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEBC8C7C8ED
        EDEDEFEFEFF3F3F3F2F2F2F2F2F2F3F3F3F3F3F3F3F3F3F4F4F4F7F7F7B1B1B2
        C0C0C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9BFBEBEEAEAEAEDED
        EDF0F0F0F1F1F1F1F1F1F2F2F2F2F2F2F2F2F2F2F2F2F4F4F4B5B5B7C2C2C3FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F3ADACACE4E5E4E7E7E7E8E8E8
        E8E8E8E8E8E8EAEAEAEAEAEAECECECEEEEEEF2F2F2AFAFB0DBDBDCFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0CACACAF1F1F1EEEEEEEDEDEDEB
        EBEBECECECEEEEEEEFEFEFF2F2F2EBEAEBB1B1B2FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFE7E7E7969695DFDFDFF2F3F3EEEEEEEDEDEDEFEF
        EFEFEFEFF2F2F2F0F0F0B2B2B4F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFD6D6D69C9B9BCFCFCFE8E8E8EEEEEEF0F0F0ECECEC
        DDDDDDB3B3B4D6D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFE9E9E9B0B0AFA4A3A3ACACABB0B0AFB0AFAFAEAEAFE5
        E5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFAFAFAE6E6E6D9D8D8DADADAE4E3E4FCFCFCFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 2
      OnClick = bbConsultarClick
    end
    object EditCaptcha: TEdit
      Left = 361
      Top = 97
      Width = 137
      Height = 41
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnKeyPress = EditCaptchaKeyPress
    end
    object EditCNPJ: TMaskEdit
      Left = 361
      Top = 31
      Width = 261
      Height = 41
      EditMask = '00.000.000/0000-00;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 18
      ParentFont = False
      TabOrder = 0
      Text = '  .   .   /    -  '
    end
    object Panel3: TPanel
      Left = 9
      Top = 11
      Width = 346
      Height = 127
      BevelOuter = bvLowered
      Color = clWhite
      ParentBackground = False
      TabOrder = 3
      object Image1: TImage
        Left = 1
        Top = 1
        Width = 344
        Height = 106
        Align = alClient
        Center = True
        Proportional = True
        Stretch = True
      end
      object LabAtualizarCaptcha: TLabel
        Left = 1
        Top = 107
        Width = 344
        Height = 19
        Cursor = crHandPoint
        Align = alBottom
        Alignment = taCenter
        AutoSize = False
        Caption = 'Atualizar Captcha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold, fsUnderline]
        ParentFont = False
        OnClick = LabAtualizarCaptchaClick
      end
    end
    object ckRemoverEspacosDuplos: TCheckBox
      Left = 10
      Top = 150
      Width = 368
      Height = 17
      Caption = 'Remover espa'#231'os duplos dos dados retornados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object bbImportar: TBitBtn
      Left = 504
      Top = 121
      Width = 98
      Height = 30
      Caption = '&Importar'
      Enabled = False
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C006000000000000000000000000000000000000FFFFFF5C8E59
        2A792B2B7F2C2C7E2D2C802D2C802D2C7D2C2C812D2C7D2C2C802D2C802D2C80
        2D2C7E2C2B7E2C2C7E2C2C7E2D2C7E2C2C7E2C2B7E2C2B7F2C2C792B5D8D58FF
        FFFF5C8F5937AF493BCF603BCD5F3CCE5F2FCB562AC85129C8502CC85128C850
        2CC85129C85029C85029C85029C8502BC8512BC8512AC9512FCA5639CE5F3DCD
        603BCF6036AE465C8E592A7C2C3BCF6139C55B39C75D39C75D21942E21942E21
        942E21942E21942E21942E21942E21942E21942E21942E21942E21942E21942E
        21942E39C75C39C75C39C65A3DCF622B7C2C2E842F3DCC623CC65D3CC85F24AC
        3EF2F1F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF2F1F224AC3B39C75C3CC55B3ACA5E2C822F2E87303ACC63
        39C65E3CC86025AB3EF2F1F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F1F224A83D39C75D39C55B3ACB622E
        842E2F88323ACD6639C76039C96226AC41F2F1F2FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F0F226AB3E3CC8
        5F38C55A3DCC622D852F2F8A323ACE6839C86339C76037C65D21942E1B91291B
        94291A91281B972D2C94340862090862091D962D1D962D1C922A1A90281C922A
        21922D37C45B39C75E3CC65E3ACC632E87302F8D3439CE6939C96738C86239C9
        653ACB663ACC673ACD673AD16C2C9434308735FFFFFFFFFFFF146B1333B24B3B
        D1673ACC653ACB633AC96239C86239C76039C7613ACD652F8A323090373AD16C
        39C96639C96638C96339C96639C96638CD6832B14C227723FFFFFFFFFFFFFFFF
        FFFCFDFE14681432B54E38CC6638C86339C86339C76339C7633CC8613ACD662F
        8B332B8E3337D06E38CB6938CB6639CB6A38CC673AD16F31B24E166E16FCFCFC
        FFFFFFFFFFFFFFFFFFFFFFFFFCFCFE1B761C35C35C38CB6638C86438C86439C9
        6737C86339CD672B8B2F5EB66E34D06C33C96537CC6939CC6A38CE6C32B65018
        711AFCFCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6E3CF126E1136C55E
        38CB6A39CC6738C86534C86134CD665EB16B7EC68D90E5B95BD69031CC6A32CE
        6B34C7630F710FFCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC9E5D013721234C55D32CB6632CA645BD48B8FE3B57BC48B7AC58A8AE4B8
        8DE3B793E4BE6ADA982D8D34CDE7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCEE7D72D8D3269D99896E4BA8DE1B48AE2B479
        C3877AC98D89E3B98BE2B586DEAC77BC7ECDE7D7D7E9DBD9E9DBD9E9DBFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDAE9DDDAE9DDD7E9DCCEE7D776B97B89DE
        AC8BE1B18CE3B57AC4897DCA8F89E3BB88E3B983D7A680D29B80D29B71CA8B71
        CA8B96CFA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF72CA8C72CA8C79D196
        7ED29A80D29B83D6A387E2B58CE3B77AC78C7DCC928CE5BD8BE3B98BE4BB8BE5
        BB8BE5BC8BE5BE85DEAE9BD0A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9C
        D2A982DEAA8CE4B989E3B789E3B68BE2B78AE1B58CE3B77CCA8F7DCE958CE6C0
        87E4BB8AE3BB8AE3BB8AE3BB8AE4BA83DDAD9FD3ABFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF9CD3A984DCAB88E3BA8BE2B787E2B78AE2B78AE2B789E4BB7D
        CB907DCF968CE6C38AE4BD8AE4BD8AE4BC8AE4BC8AE5BC83DEAE9FD5ACFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FD5AC81DDAD8BE4BA8AE3B98AE3B98AE3
        B98AE3B88CE5BD7DCD9380D1988BE8C48AE5C08AE5C08AE5BF87E4BF87E5C183
        E0B2A0D8AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2D8B180DDAE8BE5BC
        8AE4BC8AE4BB8AE4BC87E3BB8CE5BF7DCE9580D29B8BE8C68BE7C28BE7C28AE6
        C28CE7C28CE7C284E1B5A2D9B1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2
        D9B283E0B18BE5BE87E4BD8AE4C08AE4BD8AE4BD8BE6BF80D09881D49E8CEACA
        8AE7C48BE7C58AE7C48AE7C58AE8C683E0B6A7DEB9FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA5DDB885E1B68AE7C18AE5C08AE5C08AE5C08AE6C08BE7C480
        D19982D5A08BECCE8AE8C88AE8C88AE8C78AE8C78AE9C986E3BA88D49BA8DEBA
        A4DDB7A4DDB7A4DDB7A4DDB8A8DEBA87D39A85E2B68AE8C68AE6C08BE7C28AE6
        C28AE6C28DEAC880D29A94D9A78AE3BC8CEBCD8CEBCD8CEBCD8CEBCD8CEBCD8C
        EBCF8BECD089EBCF88EBCD87EBCD87EACB87EACB88EACC8AECCD8CEBCD8CEACA
        8CEACA8CEACA8CE9C98BE9C989E2B8A4DDB2FFFFFFA7E1B784DAA384D9A487DA
        A584D9A484D9A484D9A487DAA684D8A184D9A484D9A484D9A584D9A584D9A584
        D8A584D9A583D8A283D8A283D8A284D8A683D6A0A5DEB5FFFFFF}
      TabOrder = 5
      OnClick = bbImportarClick
    end
  end
  object PanelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 633
    Height = 30
    Align = alTop
    Caption = 'Consulta CNPJ [v22.12.29.1]'
    Color = 7548489
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 552
    Top = 304
  end
  object ACBrConsultaCNPJ1: TACBrConsultaCNPJ
    ProxyPort = '8080'
    PesquisarIBGE = False
    Left = 552
    Top = 362
  end
end
